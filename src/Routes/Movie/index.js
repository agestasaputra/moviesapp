import React, { Component } from 'react'
import { connect } from 'react-redux'
import { fetchDetailMovies } from '../../Redux/Actions/Movies'
import { Layout, Row, Col, Card } from 'antd'

class Movie extends Component {
	constructor(props) {
		super(props)

		this.state = {
					
		}
	}
	
	componentDidMount = async () => {
		const { id } = this.props.match.params
		await this.props.fetchDetailMovies(id)
		
	}

	render() {
		console.log('cek props: ', this.props)
		const { Movies } = this.props
		const { Content } = Layout
		
		return (
			Movies.detail ?
			<Content style={{ padding: '0 50px' }}>
        <Row style={{ padding: '24px', textAlign: 'center' }}>
          <strong> <h1> { Movies.detail.Title} </h1> </strong>
        </Row>
				<Row>
					<div style={{ background: '#fff', padding: '24px', minHeight: '70vh' }}>
						<Row>
							<Col span={6}>
								<img style={{ width: '100%' }} alt={Movies.detail.Title} src={Movies.detail.Poster} />
							</Col>
							<Col span={18} style={{ paddingLeft: '20px' }}>
								<Row>
									<Col span={4} style={{ padding: '5px' }}> <strong><p>Title</p></strong> </Col>
									<Col span={20} style={{ padding: '5px' }}> <p>: {Movies.detail.Title}</p> </Col>
								</Row>
								<Row>
									<Col span={4} style={{ padding: '5px' }}> <strong><p>IMDB Rating</p></strong> </Col>
									<Col span={20} style={{ padding: '5px' }}> <p>: {Movies.detail.imdbRating}</p> </Col>
								</Row>
								<Row>
									<Col span={4} style={{ padding: '5px' }}> <strong><p>Genre</p></strong> </Col>
									<Col span={20} style={{ padding: '5px' }}> <p>: {Movies.detail.Genre}</p> </Col>
								</Row>
								<Row>
									<Col span={4} style={{ padding: '5px' }}> <strong><p>Year</p></strong> </Col>
									<Col span={20} style={{ padding: '5px' }}> <p>: {Movies.detail.Year}</p> </Col>
								</Row>
								<Row>
									<Col span={4} style={{ padding: '5px' }}> <strong><p>Director</p></strong> </Col>
									<Col span={20} style={{ padding: '5px' }}> <p>: {Movies.detail.Director}</p> </Col>
								</Row>
								<Row>
									<Col span={4} style={{ padding: '5px' }}> <strong><p>Actors</p></strong> </Col>
									<Col span={20} style={{ padding: '5px' }}> <p>: {Movies.detail.Actors}</p> </Col>
								</Row>
								<Row>
									<Col span={4} style={{ padding: '5px' }}> <strong><p>Production</p></strong> </Col>
									<Col span={20} style={{ padding: '5px' }}> <p>: {Movies.detail.Production}</p> </Col>
								</Row>
								<Row>
									<Col span={4} style={{ padding: '5px' }}> <strong><p>Plot</p></strong> </Col>
									<Col span={20} style={{ padding: '5px' }}> <p>: {Movies.detail.Plot}</p> </Col>
								</Row>
							</Col>
							
						</Row>
					</div>
				</Row>
			</Content> :

			<Content style={{ padding: '0 50px' }}>
        <Row style={{ padding: '24px', textAlign: 'center' }}>
          <strong> <h1> Movie Detail Not Found </h1> </strong>
        </Row>
			</Content>
		)
	}
}

const mapStateToProps = ({ Movies }) => ({
  Movies
})

// export default Movie
export default connect (mapStateToProps, {
	fetchDetailMovies
})(Movie)