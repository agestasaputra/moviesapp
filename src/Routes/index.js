import React from 'react'
import { Route } from 'react-router-dom'

import Home from './Home'
import About from './About'
import Contact from './Contact'
import Movie from './Movie'

export default function index() {
	return (
		<React.Fragment>
			<Route path="/" exact component={Home} />   
			<Route path="/about" exact component={About} />   
			<Route path="/contact" exact component={Contact} />
			<Route path="/movie/:id" exact component={Movie} />
		</React.Fragment>
	)
}
